// By Squishy Enterprise
// Version 1.4
import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;

public class Snake {

	private JFrame frame;
	private EngMebb eng;
	private Canvass c;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Snake window = new Snake();
					window.frame.pack();
					window.c.requestFocusInWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Snake() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		// frame.setBounds(100, 100, 450, 300);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		c = new Canvass(frame);
		c.setForeground(new Color(128, 128, 255));
		c.setBackground(Color.BLACK);
		eng = new EngMebb(c);
		frame.getContentPane().add(c);
	}

}
