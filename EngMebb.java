// By Squishy Enterprise
// Version 1.4
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class EngMebb implements KeyListener {

	Canvass c;
	boolean paused;
	int k;

	public EngMebb(Canvass c) {
		this.c = c;
		c.addKeyListener(this);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		pressed(key);

	}

	private void pressed(int key) {
		switch (key) {
		case KeyEvent.VK_W:
			move(4);
			break;
		case KeyEvent.VK_A:
			move(3);
			break;
		case KeyEvent.VK_S:
			move(2);
			break;
		case KeyEvent.VK_D:
			move(1);
			break;
		case KeyEvent.VK_UP:
			move(4);
			break;
		case KeyEvent.VK_LEFT:
			move(3);
			break;
		case KeyEvent.VK_DOWN:
			move(2);
			break;
		case KeyEvent.VK_RIGHT:
			move(1);
			break;
		case KeyEvent.VK_G:
			k = key;
			break;
		case KeyEvent.VK_T:
			if (k == KeyEvent.VK_G) {
				c.segs[c.segments] = new Segment();
				c.segs[c.segments].x = c.segs[c.segments - 1].x;
				c.segs[c.segments].y = c.segs[c.segments - 1].y;
				c.segments++;
			}
			break;
		case KeyEvent.VK_U:
			if (k == KeyEvent.VK_G) {
				for (int i = c.segments; i < (c.maxX * c.maxY) - 1; i++) {
					c.segs[c.segments] = new Segment();
					c.segs[c.segments].x = c.segs[c.segments - 1].x;
					c.segs[c.segments].y = c.segs[c.segments - 1].y;
					c.segments++;
				}
			}
			break;
		case KeyEvent.VK_P:
			pause();
			break;
		case KeyEvent.VK_SLASH:
			c.delay = 150;
			break;
		case KeyEvent.VK_PERIOD:
			c.delay = 100;
			break;
		case KeyEvent.VK_COMMA:
			c.delay = 75;
			break;
		case KeyEvent.VK_M:
			c.delay = 50;
			break;
		case KeyEvent.VK_N:
			c.delay = 25;
			break;
		case KeyEvent.VK_B:
			c.delay = 15;
			break;
		case KeyEvent.VK_V:
			c.delay = 10;
			break;
		case KeyEvent.VK_SPACE:
			c.t.stop();
			c.start();
			paused = false;
			break;
		}
		if (key != KeyEvent.VK_G)
			res();
	}

	private void pause() {
		if (c.alive) {
			if (paused) {
				c.draw();
				paused = false;
			} else {
				c.t.stop();
				paused = true;
			}
		}
	}

	private void res() {
		k = 0;
	}

	private void move(int i) {
		c.setDir(i);
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
