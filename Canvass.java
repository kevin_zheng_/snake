// By Squishy Enterprise
// Version 1.4
import java.awt.Canvas;
import java.util.Random;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Canvass extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int dir;
	String o = new String();
	int maxX;
	int maxY;
	int delay;
	int nextMove;
	Segment[] segs;
	int segments;
	boolean move;
	boolean init;
	boolean alive;
	Segment food;
	Random r;
	Thread t;
	JFrame frame;

	public Canvass(JFrame frame) {
		super();
		this.frame = frame;
		start();
	}

	public void start() {
		System.gc();
		dir = 1;
		delay = 100;
		nextMove = 0;
		move = true;
		init = false;
		alive = true;
		food = new Segment();
		r = new Random();
		draw();
	}

	public synchronized void setDir(int dir) {
		if (Math.abs(dir - this.dir) != 2) {
			if (move) {
				this.dir = dir;
				move = false;
			} else {
				if (nextMove == 0)
					nextMove = dir;
			}
		}
	}

	public synchronized void paint(Graphics g) {
		if (!init) {
			maxX = (getWidth() / 15);
			maxY = (getHeight() / 15);
			segs = new Segment[maxX * maxY];
			segs[0] = new Segment();
			segments = 1;
			frame.setTitle("Segments: 1");
			foodGen();
			init = true;
		}
		if (alive) {
			for (int i = 0; i < segments; i++) {
				if (i == segments - 1) {
					if (segs[0].x == food.x * 15 && segs[0].y == food.y * 15) {
						segs[segments] = new Segment();
						segs[segments].x = segs[i].x;
						segs[segments].y = segs[i].y;
						segments++;
						frame.setTitle("Segments: " + segments);
						if (segments >= maxX * maxY)
							win();
						foodGen();
						break;
					}
				}
			}
			for (int i = segments - 1; i > 0; i--) {
				segs[i].x = segs[i - 1].x;
				segs[i].y = segs[i - 1].y;

			}
			move();

			g.setColor(Color.YELLOW);
			g.fillRect(food.x * 15, food.y * 15, 15, 15);
			g.setColor(Color.green);
			for (int i = 0; i < segments; i++) {
				if (i > 0)
					g.setColor(getForeground());
				g.fillRect(segs[i].x, segs[i].y, 15, 15);
			}
		}
		if (nextMove != 0) {
			int temp = nextMove;
			nextMove = 0;
			setDir(temp);
		}
	}

	private void win() {
		alive = false;
		t.stop();
		Graphics g = getGraphics();
		g.setColor(Color.WHITE);
		g.setFont(new Font("DialogInput", Font.BOLD + Font.ITALIC, 48));
		g.drawString("YOU WIN!", getWidth() / 2 - 175, getHeight() / 2 - 25);
		g.drawString("YOU HAD " + segments + " SEGMENTS", getWidth() / 2 - 175,
				getHeight() / 2 + 25);
	}

	void move() {
		switch (dir) {
		case 1:
			segs[0].x += 15;
			if (segs[0].x == maxX * 15)
				gameOver();
			for (int j = 1; j < segments; j++)
				if (segs[0].x == segs[j].x && segs[0].y == segs[j].y)
					gameOver();
			break;
		case 2:
			segs[0].y += 15;
			if (segs[0].y == maxY * 15)
				gameOver();
			for (int j = 1; j < segments; j++)
				if (segs[0].x == segs[j].x && segs[0].y == segs[j].y)
					gameOver();
			break;
		case 3:
			segs[0].x -= 15;
			if (segs[0].x == -15)
				gameOver();
			for (int j = 1; j < segments; j++)
				if (segs[0].x == segs[j].x && segs[0].y == segs[j].y)
					gameOver();
			break;
		case 4:
			segs[0].y -= 15;
			if (segs[0].y == -15)
				gameOver();
			for (int j = 1; j < segments; j++)
				if (segs[0].x == segs[j].x && segs[0].y == segs[j].y)
					gameOver();
			break;
		}
	}

	private void gameOver() {
		alive = false;
		t.stop();
		Graphics g = getGraphics();
		g.setColor(Color.WHITE);
		g.setFont(new Font("DialogInput", Font.BOLD + Font.ITALIC, 48));
		g.drawString("YOU LOSE", getWidth() / 2 - 175, getHeight() / 2 - 25);
		g.drawString("YOU HAD " + segments + " SEGMENTS", getWidth() / 2 - 175,
				getHeight() / 2 + 25);
	}

	public void foodGen() {
		food.x = r.nextInt(maxX);
		food.y = r.nextInt(maxY);
		for (int i = 0; i < segments; i++)
			if (segs[i].x == food.x * 15 && segs[i].y == food.y * 15)
				foodGen();
	}

	public void draw() {
		t = new Thread(new Runnable() {
			public void run() {
				synchronized (o) {
					try {
						while (true) {
							o.wait(delay);
							repaint();
							move = true;
							if (o.equals(" "))
								break;
						}
					} catch (Exception asdf) {
					}
				}
			}
		});
		t.start();
	}
}
