Version 1.4 Changelog

- Added a cheat to increase snake length
- Made it so one no longer needs to click to begin (Canvas automatically gets focus)
- Fixed bug allowing one to continue playing after death (using pause or resizing window)
- Fixed bug causing fast speeds if game is restarted when paused
- Changed speed values
- No longer performs some redundent drawings
- Added a cheat to get (winning number - 1) segments
- Added a win screen when number of snake segments is equal to number of blocks on screen
- Moved segment counter to top bar of frame
- Fixed going back on yourself
- Harvey Seal of Approval given